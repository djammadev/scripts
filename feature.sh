#!/bin/bash

function usage() {
    echo "Usage: feature.sh -b baseBranch -f feature [-m message to add to readme file. ]" 1>&2;
    echo "       -b base branch e.g. devcc, devcrm, master"
    echo "       -f feature ticket number, CCC-XXX, CBSD-XXX"
    echo "       -m message to add to README"
    echo "       Note : please execute this script in project directory"
    exit 1;
}

baseBranch='';
prodAlias='';
feature='';
message=''
createScratchOrg=''

while getopts "b:f:m:h" option;
do
    case "${option}" in
        b)
            baseBranch=${OPTARG}
            ;;
        f)
            feature=${OPTARG}
            ;;
        m)
            message=${OPTARG}
            ;;
        h)
            usage
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

function exitIfError() {
    if [ $1 != 0 ]
    then
        echo $2
        # do something here to address the error
        exit $1
    fi
}

function warnIfError() {
    if [ $1 != 0 ]
    then
        echo $2
    fi
}

if [ -z $feature ]
then
    echo "feature is required";
    usage;
fi

#if [ -z $prodAlias ]
#then
#    echo "Production alias is required";
#    usage;
#fi

echo "Using base branch $baseBranch for feature $feature";

TODAY=`date +'%Y-%m-%d'`

echo "Creating today working directory"

mkdir -p mdapi/Technical-Docs 1>&2;

echo "Creating feature directory"
mkdir -p mdapi/$TODAY/$feature 1>&2;

README=mdapi/Technical-Docs/$feature-README.md;

function addInfo() {
    echo $@  >> $README;
}

function generateReadme() {
    echo "Creating feature README.md file.";
    addInfo "# [$feature](https://guimini.atlassian.net/browse/$feature)";
    addInfo "";
    addInfo "## Purpose"
    addInfo ""
    addInfo "## Apex Classes"
    addInfo "|Name|Comment|"
    addInfo "|--|--|"
    addInfo ""
    addInfo "## Apex Pages"
    addInfo "|Name|Comment|"
    addInfo "|--|--|"
    addInfo ""
    addInfo "## Custom Fields"
    addInfo "|Label|Parent|Name|Comment|"
    addInfo "|--|--|--|--|"
    addInfo ""
    addInfo "## Lightning Component Bundle"
    addInfo "|Name|Comment|"
    addInfo "|--|--|"
}

CURRENT_BRANCH=`git branch | grep '*'`
FEATURE_BRANCH=`git branch | grep ${baseBranch}_${feature}`
echo "Your current branch is $CURRENT_BRANCH";

function createFeatureBranch() {
    if [ "$CURRENT_BRANCH" = "* $baseBranch" ]
    then
        echo "You are on your base branch $baseBranch".
    else
        echo "Updating base branch..."
        git checkout $baseBranch
    fi
    git pull
    exitIfError $? "Unable to update base branch."
    echo "Checking out feature branch"
    if [ -z "$FEATURE_BRANCH" ]
    then
        git checkout -b ${baseBranch}_${feature}
        exitIfError $? "Unable to create new branch."
    else
        echo "Feature branch already exist."
        git checkout ${baseBranch}_${feature}
        echo "Rebasing on ${baseBranch}"
        git rebase ${baseBranch}
    fi
}

if [ -f $README ]
then
    echo "README File already exists"
else
    generateReadme
fi

DRAFT=`git diff`

if [ -z "$message" ]
then
    if [ -z $DRAFT ]
    then
        createFeatureBranch
        isToday=`cat $README | grep "$TODAY"`
        echo $isToday;
        if [ -z "$isToday" ]
        then
            addInfo "### $TODAY"
        fi
    else
        echo "You need to clean your working directory";
        echo "Working in progress..."
        echo $DRAFT;
    fi
else
    addInfo ""
    addInfo $message;
fi
echo "Done."
