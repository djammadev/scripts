#!/usr/bin/env bash
if [ -z $HOST ]
then
  HOST=${CI_PROJECT_URL}
fi
# Extract the host where the server is running, and add the URL to the APIs
[[ $HOST =~ ^https?://[^/]+ ]] && HOST="${BASH_REMATCH[0]}/api/v4/projects/"

function help() {
    echo "gitlabci.sh [action : create_merge, validate_merge, create_tag, get_tag, delete_tag] [-t target-branch (default branch)] [-r remove-source-branch (false)] [--merge-when-pipelines-succeeds (false)] [-h help]"
}

function exitIfError() {
    if [ ! $1 -eq 0 ]
    then
        echo "$2"
        exit $1;
    fi
}

if [ -z $CI_PROJECT_URL ] && [ -z $CI_PROJECT_ID ] && [ -z $PRIVATE_TOKEN ]
then
  echo "Please set env variables CI_PROJECT_URL, CI_PROJECT_ID, PRIVATE_TOKEN"
  echo "CI_PROJECT_URL, CI_PROJECT_ID, PRIVATE_TOKEN = $CI_PROJECT_URL, $CI_PROJECT_ID, $PRIVATE_TOKEN"
  exit 1
fi

SOURCE_BRANCH=${CI_COMMIT_REF_NAME}
# Look which is the default branch
TARGET_BRANCH=`curl --silent "${HOST}${CI_PROJECT_ID}" --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}" | python3 -c "import sys, json; print(json.load(sys.stdin)['default_branch'])"`;
# TARGET_BRANCH=develop
REMOVE_SOURCE_BRANCH=false
MERGE_WHEN_PL_SUCCEEDS=false
MERGE_WHEN_PL_SUCCEEDS_ONLY=false

if [ $SOURCE_BRANCH = $TARGET_BRANCH ]
then
  # We have a merge request
  SOURCE_BRANCH=$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME
  TARGET_BRANCH=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME
fi

SHORT="s:t:T:n:rmMhp:"
LONG="--source-branch:,--target-branch:,--tag-type:,--tag-prefix:,--tag-name:,--remove-source-branch,--merge-when-pipelines-succeeds,--merge-when-pipelines-succeeds-only,--help"
# OPTS=$(getopt --options $SHORT --long $LONG --name "$0" -- "$@")

eval set -- "$@ --"

exitIfError $? "Error occur before parsing args."

ACTION=$1
shift 1
TAG_TYPE=build
TAG_PREFIX=''

while true ;
do
case "$1" in
    -t | --target-branch )
      TARGET_BRANCH=$2
      shift 2
      ;;
    -s | --source-branch )
      SOURCE_BRANCH=$2
      shift 2
      ;;
    -T | --tag-type )
      TAG_TYPE=$2
      shift 2
      ;;
    -p | --tag-prefix )
      TAG_PREFIX=$2
      shift 2
      ;;
    -n | --tag-name )
      TAG_NAME=$2
      shift 2
      ;;
    -r | --remove-source-branch )
      REMOVE_SOURCE_BRANCH=true
      shift 1
      ;;
    -m | --merge-when-pipelines-succeeds )
      MERGE_WHEN_PL_SUCCEEDS=true
      shift 1
      ;;
    -M | --merge-when-pipelines-succeeds-only )
      MERGE_WHEN_PL_SUCCEEDS_ONLY=true
      shift 1
      ;;
    -h | --help )
      help;
      exit 1;
      ;;
    -- )
      shift
      break
      ;;
    *)
      echo "$1"
      help;
      exit 1
      ;;
esac
done

if [ -z $TAG_PREFIX ]
then
    TAG_PREFIX=${TARGET_BRANCH}-${TAG_TYPE}-
fi

function merge_when_pipeline_succeeds() {
  PUT_BODY="{
            \"id\": ${CI_PROJECT_ID},
            \"merge_request_iid\":${CI_MERGE_REQUEST_IID},
            \"merge_when_pipeline_succeeds\": true
             }"
  echo "PUT_BODY             = $PUT_BODY"
  echo "${HOST}${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}/merge"
  curl -X PUT "${HOST}${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}/merge" \
        --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}" \
        --header "Content-Type: application/json" \
        --data "${PUT_BODY}"
}

function createMergeRequest() {
  BODY="{
    \"id\": ${CI_PROJECT_ID},
    \"source_branch\": \"${SOURCE_BRANCH}\",
    \"target_branch\": \"${TARGET_BRANCH}\",
    \"remove_source_branch\": ${REMOVE_SOURCE_BRANCH},
    \"merge_when_pipeline_succeeds\": ${MERGE_WHEN_PL_SUCCEEDS},
    \"title\": \"MR ${SOURCE_BRANCH} -> ${TARGET_BRANCH}\",
    \"assignee_id\":\"${GITLAB_USER_ID}\"
  }";

  echo "SOURCE_BRANCH        = $SOURCE_BRANCH"
  echo "TARGET_BRANCH        = $TARGET_BRANCH"
  echo "REMOVE_SOURCE_BRANCH = $REMOVE_SOURCE_BRANCH"
  echo "BODY                 = $BODY"

  # Require a list of all the merge request and take a look if there is already
  # one with the same source branch
  LISTMR=`curl --silent "${HOST}${CI_PROJECT_ID}/merge_requests?state=opened" --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}"`;
  COUNTBRANCHES=`echo ${LISTMR} | grep -o "\"target_branch\":\"${TARGET_BRANCH}\",\"source_branch\":\"${SOURCE_BRANCH}\"" | wc -l`;

  exitIfError $? "Error when looking for merge requests list."

  # No MR found, let's create a new one
  if [ ${COUNTBRANCHES} -eq "0" ]; then
      response=`curl -X POST "${HOST}${CI_PROJECT_ID}/merge_requests" \
          --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}" \
          --header "Content-Type: application/json" \
          --data "${BODY}"`;
      echo "curl -X POST : $response"
      exitIfError $? "Merge request failed."
      echo "Opened a new merge request: ${SOURCE_BRANCH} and assigned to you";
      if [ $MERGE_WHEN_PL_SUCCEEDS = true ]
      then
          iid=`echo $response | python3 -c "import sys, json; print(json.load(sys.stdin)['iid'])"`
          PUT_BODY="{
              \"id\": ${CI_PROJECT_ID},
              \"merge_request_iid\":${iid},
              \"merge_when_pipeline_succeeds\": true
              }"
          echo "iid                  = $iid"
          echo "PUT_BODY             = $PUT_BODY"
          curl -X PUT "${HOST}${CI_PROJECT_ID}/merge_requests/${iid}/merge" \
          --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}" \
          --header "Content-Type: application/json" \
          --data "${PUT_BODY}"
      fi
      exit;
  fi
}

function get_tag() {
  tags=`curl --silent "${HOST}${CI_PROJECT_ID}/repository/tags?order_by=name&sort=desc&search=^${TAG_PREFIX}" --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}"`
  tagCount=`echo $tags | python3 -c "import sys, json; print(len(json.load(sys.stdin)))"`
  if [ $tagCount -gt 0 ]
  then
    tag=`echo $tags | python3 -c "import sys, json; print(json.load(sys.stdin)[0]['name'])"`
    echo $tag
  else
    echo $TARGET_BRANCH
  fi
}

function create_tag() {
  echo 'Creating tag...'
  response=`curl --silent --request POST --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}" "${HOST}${CI_PROJECT_ID}/repository/tags?tag_name=${TAG_PREFIX}${CI_PIPELINE_IID}&ref=${TARGET_BRANCH}"`
  error=`echo $response | grep -o "already exists" | wc -l`
  if [ $error -eq 0 ]
  then
    tag=`echo $response | python3 -c "import sys, json; print(json.load(sys.stdin)['name'])"`
    echo "$tag created."
  else
    echo $response | python3 -c "import sys, json; print(json.load(sys.stdin)['message'])"
  fi
}

function delete_tag() {
  echo 'Deleting tag...'
  response=`curl --silent --request DELETE --header "PRIVATE-TOKEN:${PRIVATE_TOKEN}" "${HOST}${CI_PROJECT_ID}/repository/tags/${TAG_NAME}"`
  echo $response
}

if [ "X$ACTION" = "Xvalidate_merge" ]
then
  merge_when_pipeline_succeeds
  exit;
fi
if [ "X$ACTION" = "Xcreate_merge" ]
then
  createMergeRequest
fi

if [ "X$ACTION" = "Xget_tag" ]
then
  get_tag
fi

if [ "X$ACTION" = "Xcreate_tag" ]
then
  create_tag
fi

if [ "X$ACTION" = "Xdelete_tag" ]
then
  delete_tag
fi
