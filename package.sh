#!/bin/bash
# In Customer Care we have many sandboxes, in order to accelerate our dev we need to deploy quickly on these sandboxes.
# This script aim to generate a mdapi package and validate/deploy on a specified sandboxe/scratch-org.
# If we have to fix quickly a problem on a sanbox, we don't want to wait for all build process to deploy on a sandbox to test.

function usage() {
    echo "Usage: package.sh -s source -t target [-u target org] [-o output] [-i no interact] [-c checkonly] [-l test level] [-r test list]"
    echo "       -s source branch or revision (mandatory)"
    echo "       -t target branch or revision (mandatory)"
    echo "       -u target org"
    echo "       -o output dir (default main)"
    echo "       -y default yes to backup the metadata first and to validate the package ."
    echo "       -n default no to  backup the metadata first and to validate the package ."
    echo "       -i no interact by asking to decide"
    echo "       -c checkonly"
    echo "       -l Test Level"
    echo "       -r Test Classes List"
    echo "\n"
    echo "Example"
    echo "package.sh -s master -t devcc -c -y -u preprod"
    exit 1;
}

source=""
target=""
username=""
alias=""
interact="yes"
output="main"
checkonly=""
testArgs=""
testLevel=""
yes=""

while getopts "s:t:u:po:ichl:r:ny" option;
do
    case "${option}" in
        s)
            source=${OPTARG}
            ;;
        t)
            target=${OPTARG}
            ;;
        u)
            username="-u ${OPTARG}"
            alias=${OPTARG}
            ;;
        i)
            interact="no"
            ;;
        y)
            interact="no"
            yes="yes"
            ;;
        n)
            interact="no"
            yes="no"
            ;;
        c)
            checkonly="-c"
            ;;
        o)
            output=${OPTARG}
            ;;
        l)
            testLevel="${OPTARG}"
            ;;
        r)
            testArgs="${testArgs}-${option} ${OPTARG} "
            if [ -z $testLevel ]
            then
                testLevel="RunSpecifiedTests"
            fi
            ;;
        h)
            usage
            ;;
        *)
            testArgs="${testArgs}-${option} ${OPTARG} "
            ;;
    esac
done
shift $((OPTIND-1))

if [ ! -z $testLevel ]
then
    testArgs="${testArgs}-l $testLevel "
fi

TODAY=`date +'%Y-%m-%d'`
mkdir -p mdapi/$TODAY 1>&2;

PACKAGE_DIR=mdapi/$TODAY;

function exitIfError() {
    if [ $1 != 0 ]
    then
        echo $2
        # do something here to address the error
        # rm -rf force-app/*;
        exit $1
    fi
}

function split() {
    echo $(echo $1 | tr $2 "\n")
}

function gitbranch() {
    git branch | sed -n '/\* /s///p'
}

if [ -z $target ]
then
    target=`gitbranch`;
fi

function checkSource() {
    if [ -z $source ]
    then
        currentBranch=`gitbranch`
        args=`split $currentBranch _`
        set -- $args;
        echo $@;
        source=$1
    fi
    echo "Source = $source"
}
checkSource;

function checkUsername() {
    if [ -z "$username" ]
    then
        currentBranch=`gitbranch`
        args=`split $currentBranch _`
        set -- $args;
        echo $@;
        username="-u $1"
        alias=$1
    fi
    echo "Username = $username"
}
checkUsername;

mkdir  -p "$output" 1>&2;
GIT_DIFF="${source}_${target}.diff"

SOURCE_BRANCH=`git branch | grep "${source}"`
if [ ! -z "$SOURCE_BRANCH" ]
then
    echo "Updating source branch"
    git checkout $source
    exitIfError $? "Git checkout ${source} failed"
    git pull origin $source
    exitIfError $? "Git Pull ${source} failled failed"
else
    git fetch origin $source
    exitIfError $? "Git fetch ${source} failed failed"
    git checkout $source
    exitIfError $? "Git checkout ${source} failed"
fi

# Checkout only if target branch exists.
TARGET_BRANCH=`git branch | grep "${target}"`
if [ ! -z "$TARGET_BRANCH" ]
then
    echo "Checking out target branch."
    git checkout $target;
    exitIfError $? "Git checkout ${target} failed"
else
    git fetch origin $target
    exitIfError $? "Git fetch ${target} failed"
    git checkout $target
    exitIfError $? "Git checkout ${target} failed"
    echo "Generating diff for a target revision."
fi

echo "Generating git diff into $GIT_DIFF"
git diff $source $target --raw > $GIT_DIFF
echo "Generating a source project with $GIT_DIFF"

if [ ! -z "`ls $output`" ]
then
    echo "output is not empty, removing its content."
    rm -rf $output/force-app;
fi

#backup permissions
# backUpPermissions 

# Generate diff
sfdx dforce:project:diff -d $output -f $GIT_DIFF
exitIfError $? "sfdx dforce:project:diff command failed."
echo "Constructing project"

#cp -r sfdx-project.json $output/ 
#HAS_PROFILES=`cat $GIT_DIFF | grep '/profiles/'`
#HAS_PERMSETS=`cat $GIT_DIFF | grep '/permissionsets/'`

#cd $output

#if [ ! -z $HAS_PROFILES] || [ ! -z $HAS_PERMSETS]
#then
    #reconcile profiles
    #reconcile $alias
#else
#    echo "Skipping reconcile"
#fi
#mkdir -p force-app; # If force-app does not exist.
#echo "Before: Clean up force-app"
#rm -rf force-app/*

#sfdx acnforce:project:build -d force-app

if [ -d $output/force-app ]
then
    packageName=${alias}_${source}_${target}
    mkdir -p $PACKAGE_DIR/$packageName;
    echo "rm -rf $PACKAGE_DIR/$packageName/*"
    rm -rf $PACKAGE_DIR/$packageName/*;
    echo "Converting to mdapi format"
    sfdx force:source:convert -r $output/force-app -d $PACKAGE_DIR/$packageName

    #cd ..

    exitIfError $? "sfdx force:source:convert command failed."
    if [ ! -z "`ls destructiveChanges*`" ]
    then
        cp -r destructiveChanges* $PACKAGE_DIR/$packageName/
    fi
fi
function retrievePackage() {
    sfdx force:mdapi:retrieve -k $1 -r $PACKAGE_DIR/${packageName}_backup $username
    exitIfError $? "sfdx force:mdapi:retrieve -k $1 command failed."
    cd $PACKAGE_DIR/${packageName}_backup;
    unzip unpackaged.zip
    mv unpackaged/package.xml unpackaged/`basename $1`
    cd -;
}

function validate() {
    if [ $interact = "yes" ]
    then
        echo "Do you want to create a backup before the package to be deployed y/n?"
        read yes;
    fi
    if [ $yes = "y" ] || [ $yes = "yes" ]
    then
        echo "Creating a backup"
        mkdir -p $PACKAGE_DIR/${packageName}_backup;
        rm -rf $PACKAGE_DIR/${packageName}_backup/*;
        for pack in `ls $PACKAGE_DIR/${packageName}/*.xml`
        do
            retrievePackage $pack;
        done
        mv $PACKAGE_DIR/${packageName}_backup/unpackaged/* $PACKAGE_DIR/${packageName}_backup/
        rm -rf $PACKAGE_DIR/${packageName}_backup/unpackaged*
    fi
    echo "Sending"
    sfdx force:mdapi:deploy -d $PACKAGE_DIR/${packageName} $username $checkonly $testArgs;
    exitIfError $? "sfdx force:mdapi:deploy command failed."
}

if [ -d $output/force-app ]
then
    if [ $interact = "yes" ]
    then
        echo "Do you want to validate new package on target org y/n?"
        read yes;
    fi
    if [ $yes = "yes" ] || [ $yes = "y" ]
    then
        validate;
    fi
    rm -rf $output/force-app
fi
echo "After: Clean up force-app"
echo "Clean up $GIT_DIFF"
rm $GIT_DIFF;

echo "Done."