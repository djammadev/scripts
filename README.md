# Djamma Dev Scripts

## gitlabci.sh

Interaction with gitlab.

```sh
gitlabci.sh [commands] [options]
    commands:
        create_merge
        validate_merge
        get_tag
        create_tag
        delete_tag
    options:
        -s, --source-branch source branch for merge request.
        -t, --target-branch (default branch)
        -r, --remove-source-branch (false)
        -m, --merge-when-pipelines-succeeds (false)
        -M, --merge-when-pipelines-succeeds-only (false)
        -T, --tag-type (Tag type : build, release) : (build) naming convention [branch]-[tag-type]-[pipeline-iid]
        -p, --tag-prefix (replace [branch]-[tag-type]) : custom naming convention on tag.
        -n, --tag-name (Tag name to delete)
        -h, --help : show help message
```

## feature.sh

Branch management

```sh
echo "Usage: feature.sh -b baseBranch -f feature [-m message to add to readme file. ]" 1>&2;
echo "       -b base branch e.g. develop, master"
echo "       -f feature ticket number, [JIRA-PROJECT]-XXX"
echo "       -m message to add to README"
echo "       Note : please execute this script in project directory"
```

## package.sh

Project package generation

```sh
echo "Usage: package.sh -s source -t target [-u target org] [-o output] [-i no interact] [-c checkonly] [-l test level] [-r test list]"
echo "       -s source branch or revision (mandatory)"
echo "       -t target branch or revision (mandatory)"
echo "       -u target org"
echo "       -o output dir (default main)"
echo "       -y default yes to backup the metadata first and to validate the package ."
echo "       -n default no to  backup the metadata first and to validate the package ."
echo "       -i no interact by asking to decide"
echo "       -c checkonly"
echo "       -l Test Level"
echo "       -r Test Classes List"
echo "\n"
echo "Example"
echo "      package.sh -s master -t develop -c -y -u mysandbox"
```