#!/usr/bin/env bash

function printLog() {
    echo "[`date +'%Y-%m-%d %H:%M:%S'`] $@"
}

function help() {
    echo "checkstatus.sh [-d baseDir] [-a app-name] [-p port] [-s slack-url] [-c slack-channel] [-r auto-reload] [-h help]"
    echo "$SHORT"
    echo "$LONG"
}

printLog "checkstatus.sh $@"

if [ $# = 0 ]
then
    help
    exit -1
fi

BASE_DIR=$HOME/Developer/Projects/DjammaDev
APPNAME=$1
SLACK_URL="https://hooks.slack.com/services/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" # Djamma Dev
SLACK_USER=$APPNAME
SLACK_CHANNEL="#servers"
AUTO_RELOAD=false

function exitIfError() {
    if [ ! $1 -eq 0 ]
    then
        echo "$2"
        exit $1;
    fi
}

SHORT="d:a:p:c:s:r:h"
LONG="--baseDir:,--app-name:,--port:,--slack-url:,--slack-channel:,--auto-reload,--help"

eval set -- "$@ --"

exitIfError $? "Error occur before parsing args."

while true ;
do
case "$1" in
    -d | --baseDir )
      BASE_DIR=$2
      shift 2
      ;;
    -a | --app-name )
      APPNAME=$2
      shift 2
      ;;
    -p | --port )
      PORT=$2
      shift 2
      ;;
    -s | --slack-url )
      SLACK_URL=$2
      shift 2
      ;;
    -c | --slack-channel )
      SLACK_CHANNEL=$2
      shift 2
      ;;
    -r | --auto-reload )
      AUTO_RELOAD=true
      shift 1
      ;;
    -- )
      shift
      break
      ;;
    *)
      echo "$1"
      help;
      exit 2
      ;;
esac
done

if [ -z $BASE_DIR ] || [ -z $APPNAME ]
then
    help
    echo "baseDir is required"
    echo "app-name is required"
fi

APP_DIR=$BASE_DIR/$APPNAME
if [ -z $PORT ]
then
    PORT=`cat $APP_DIR/PORT`
fi

if [ -r $APP_DIR/setenv.sh ]
then
    . $APP_DIR/setenv.sh
fi

app_state=`lsof -i ":$PORT"`

state="0"
if [ -f $APP_DIR/.state ]
then
    state=`cat $APP_DIR/.state`
else
    echo "0" > $APP_DIR/.state
fi

function sendNotification() {
    curl -X POST --data-urlencode "payload={\"channel\": \"$SLACK_CHANNEL\", \"username\": \"$SLACK_USER\", \"text\": \"$1\"}" $SLACK_URL
    echo $2 > $APP_DIR/.state
}

if [ ! -z "$app_state" ]
then
    if [ $state -eq 1 ]
    then
        sendNotification "Server[$APPNAME] started on $PORT" 0
    fi
else
   if [ $state -eq 0 ]
   then
        sendNotification "Server[$APPNAME] stopped on $PORT" 1
   fi
fi

if [ $AUTO_RELOAD = true ] && [ -z "$app_state" ]
then
    jersey restart -d $BASE_DIR -a $APPNAME
fi
