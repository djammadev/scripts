#!/usr/bin/env bash

HTTP_CONFIG='server {
    listen 80;
    listen [::]:80;
    server_name mydomain.com;

    root /var/www/mydomain.com;

    location ~ /\.well-known/acme-challenge {
        allow all;
    }

    location / {
        return 301 https://mydomain.com$request_uri;
    }
}'

HTTPS_CONFIG='server {

    listen 443 ssl spdy;
    listen [::]:443 ssl spdy;
    spdy_headers_comp 9;

    server_name mydomain.com;

    root /var/www/mydomain.com;
    index index.html index.htm;

    error_log /var/log/nginx/mydomain.com.log notice;
    access_log off;

    location ~ /\. { deny all; }

    location /api {
        proxy_pass              http://127.0.0.1:proxy-port;
        proxy_set_header        Host               $host;
        proxy_set_header        X-Real-IP          $remote_addr;
        proxy_set_header        X-Forwarded-Proto  https;
        proxy_set_header        X-Forwarded-Ssl    on;
        proxy_set_header        X-Forwarded-For    $proxy_add_x_forwarded_for;
        # WebSocket support (nginx 1.4)
        proxy_http_version      1.1;
        proxy_set_header        Upgrade $http_upgrade;
        proxy_set_header        Connection "upgrade";
        proxy_set_header        X-Origin "https://mydomain.com";
    }

    ssl on;
    ssl_certificate /etc/letsencrypt/live/mydomain.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/mydomain.com/privkey.pem;

    ssl_stapling on;
    ssl_stapling_verify on;
    ssl_trusted_certificate /etc/letsencrypt/live/mydomain.com/fullchain.pem;
    resolver 8.8.8.8 8.8.4.4 208.67.222.222 208.67.220.220 216.146.35.35 216.146.36.36 valid=300s;
    resolver_timeout 3s;

    ssl_ecdh_curve secp384r1;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_prefer_server_ciphers on;
    ssl_ciphers "ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!3DES:!MD5:!PSK";
}'


SHORT="d:a:p:g:w:c"
LONG="--domain:,--app-root:,--proxy-port:,--nginx-root:,--web-root:,--no-cert,--help"

function print() {
    echo "[`date +'%Y-%m-%d %H:%M:%S'`] $@"
}

function help() {
    echo "djammadev.sh [-d domain] [-a app-root] [-p Proxy port] [-g nginx-root] [-w web-root] [-c do not generate certificate] [-h help]"
    echo "$SHORT"
    echo "$LONG"
}

function exitIfError() {
    if [ ! $1 -eq 0 ]
    then
        echo "$2"
        exit $1;
    fi
}

eval set -- "$@ --"

exitIfError $? "Error occur before parsing args."

DOMAIN=''
PROXY_PORT=''
APP_ROOT=''
NGINX_ROOT='/etc/nginx'
WEB_ROOT='/var/www'
NO_CERT=false

while true ;
do
case "$1" in
    -d | --domain )
      DOMAIN=$2
      shift 2
      ;;
    -a | --app-root )
      APP_ROOT=$2
      shift 2
      ;;
    -p | --proxy-port )
      PROXY_PORT=$2
      shift 2
      ;;
    -s | --proxy-server )
      PROXY_SERVER=$2
      shift 2
      ;;
    -g | --nginx-root )
      NGINX_ROOT=$2
      shift 2
      ;;
    -w | --web-root )
      WEB_ROOT=$2
      shift 2
      ;;
    -c | --no-cert )
      NO_CERT=true;
      shift 1
      ;;
    -h | --help )
      help;
      exit 1;
      ;;
    -- )
      shift
      break
      ;;
    *)
      echo "$1"
      help;
      exit 2
      ;;
esac
done

if [ -z $DOMAIN ]
then
    print "domain is required"
    help;
    exit 2;
fi

if [ -z $APP_ROOT ]
then
    print "app-root is required"
    help;
    exit 2;
fi

print "Setting proxy"
if [ -z $PROXY_PORT ]
then
    print "proxy port required"
    help;
    exit 2;
fi
# print $HTTPS_CONFIG
print "Setting domain configuration"
HTTP_CONFIG=`echo "$HTTP_CONFIG" | sed 's/mydomain.com/'"$DOMAIN"'/g'`
exitIfError $? "sed command failed HTTP_CONFIG"

print "Linking app-root to web-root"
print "ln -sFf $APP_ROOT/$DOMAIN $WEB_ROOT/$DOMAIN"
ln -sFf $APP_ROOT/$DOMAIN $WEB_ROOT/$DOMAIN

#print "mkdir -p $NGINX_ROOT/sites-available"
#mkdir -p $NGINX_ROOT/sites-available
#exitIfError $? "creating sites-available dir failed"
#print "mkdir -p $NGINX_ROOT/sites-enabled"
#mkdir -p $NGINX_ROOT/sites-enabled;
#exitIfError $? "creating sites-enabled dir failed"

print "adding configuration to $NGINX_ROOT/sites-available/$DOMAIN"
echo "$HTTP_CONFIG" > $NGINX_ROOT/sites-available/$DOMAIN
exitIfError $? "creating site failed"

print "ln -sFf ../sites-available/$DOMAIN $NGINX_ROOT/sites-enabled/$DOMAIN"
ln -sFf ../sites-available/$DOMAIN $NGINX_ROOT/sites-enabled/$DOMAIN
exitIfError $? "ln failed"

# nginx
print "nginx -s reload"
nginx -s reload
exitIfError $? "nginx failed"

if [ $NO_CERT = false ]
then
    HTTPS_CONFIG=`echo "$HTTPS_CONFIG" | sed 's/proxy-port/'"$PROXY_PORT"'/g'`
    exitIfError $? "sed command failed HTTPS_CONFIG"
    HTTPS_CONFIG=`echo "$HTTPS_CONFIG" | sed 's/mydomain.com/'"$DOMAIN"'/g'`
    exitIfError $? "sed command failed HTTPS_CONFIG"
    # letsencrypt
    print "letsencrypt certonly --rsa-key-size 4096 --webroot --webroot-path $WEB_ROOT/$DOMAIN -d $DOMAIN"
    letsencrypt certonly --rsa-key-size 4096 --webroot --webroot-path $WEB_ROOT/$DOMAIN -d $DOMAIN
    exitIfError $? "letsencrypt failed"

    print "echo "$HTTPS_CONFIG" >> $NGINX_ROOT/sites-available/$DOMAIN"
    echo "$HTTPS_CONFIG" >> $NGINX_ROOT/sites-available/$DOMAIN
    exitIfError $? "adding https config failed"
    print "Reload nginx again."
    print "nginx -s reload"
    nginx -s reload
    exitIfError $? "nginx failed"
fi
print "Done.."
